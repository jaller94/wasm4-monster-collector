# WASM-4 Monster Collector

A Pokemon-like video game demo for the [WASM-4 fantasy console](https://wasm4.org/).

![](./docs/overworld-town.webm)

[Play it on the web!](https://jaller94.gitlab.io/wasm4-monster-collector)

* All code is published under the MIT License.
* All assets are published under CC0.
  * [Tileset](https://opengameart.org/content/gameboy-tileset) by ArMM1998
  * [Character](https://opengameart.org/content/blowhard-chronicles) by surt

## Known issues

* Tile collision is inaccurate around fences and bridges.
* There's no caching or draw skipping.
